#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <curses.h>
#include <assert.h>
#define  NSPIS  5                                 /*����.������ ����.�����. */
#define  NOBJ   50                                /*����.����.��'������ ����*/
#define  DOBLZ  1024                              /*����� ������� ��������  */

WINDOW *wblue, *wgreen, *wred, *wcyan, *wmargenta, *yawblue;

struct STR_BUF_TXT
{
  unsigned char POLE1      ;                      /*����� ��� ���� 0x02     */
  unsigned char POLE2  [ 3];                      /*���� ���� ��'����.����� */
  unsigned char POLE3      ;                      /*������                  */
  unsigned char ADOP   [ 3];                      /*�������.����� ��������  */
  unsigned char POLE5  [ 2];                      /*�������                 */
  unsigned char DLNOP  [ 2];                      /*����� ��������          */
  unsigned char POLE7  [ 2];                      /*�������                 */
  unsigned char POLE71 [ 2];                      /*���������� �����.�����. */
  unsigned char OPER   [56];                      /*���� ��������           */
  unsigned char POLE9  [ 8];                      /*����������������� ����  */
};

union                                             /*���������� ��'��������  */
{
  struct STR_BUF_TXT STR_TXT;                     /*��������� ������        */
  unsigned char BUF_TXT [80];                     /*����� ����� TXT         */
} TXT;


unsigned char INST [6]; /*������, ������. �������. �������                 */
const int INST_SIZE = sizeof(INST);



int FRR();
int FRX();

struct
{
  int R1; /*����� 1-�� ��������-�������� � �������� RR � RX*/
  int R2; /*����� 2-�� ��������-�������� � ������� RX      */
  int D;  /*�������� � ������� RX   */
  int X;  /*����� �������. �������� � ������� RX            */
  int B;  /*����� �������� �������� � ������� RX            */
} RR_RX;

unsigned long CURR_ADDR, LIGHT_ADDR;
unsigned long VR[16];

union
{
  struct
  {
    float VALUE;
    float FILLER;
  } FLOAT_VR;
  struct
  {
    double VALUE;
  } DOUBLE_VR;
} FVR[4];

union
{
  unsigned int BAS_ADDR;
  unsigned char *P_OBLZ ;
} POINT;
unsigned char OBLZ[DOBLZ] ;                    /*������� �������� ����-  */
/*�������� ���������      */
/*
***** ������� �������� ��������
*/
int P_BALR();
int P_BCR();
int P_ST();
int P_L();
int P_A();
int P_S();
int P_STE();
int P_LE();
int P_AE();
int P_SE();

struct TMOP                                      /*������.���.����.���.����*/
{
  unsigned char MNCOP [5];                       /*�������� ��������       */
  unsigned char CODOP    ;                       /*�������� ��� ��������   */
  unsigned char DLOP     ;                       /*����� �������� � ������ */
  int (*BXPROG)()        ;
  int (*YABXPROG)();
} T_MOP []  =                                /*��'������� ����.���.����*/
{
  {{'B' , 'A' , 'L' , 'R' , ' '} , '\x05', 2 , FRR, P_BALR},
  {{'B' , 'C' , 'R' , ' ' , ' '} , '\x07', 2 , FRR, P_BCR},
  {{'S' , 'T' , ' ' , ' ' , ' '} , '\x50', 4 , FRX, P_ST},
  {{'L' , ' ' , ' ' , ' ' , ' '} , '\x58', 4 , FRX, P_L},
  {{'A' , ' ' , ' ' , ' ' , ' '} , '\x5A', 4 , FRX, P_A},
  {{'S' , ' ' , ' ' , ' ' , ' '} , '\x5B', 4 , FRX, P_S},
  {{'S' , 'T' , 'E' , ' ' , ' '} , '\x70', 4 , FRX, P_STE},
  {{'L' , 'E' , ' ' , ' ' , ' '} , '\x78', 4 , FRX, P_LE},
  {{'A' , 'E' , ' ' , ' ' , ' '} , '\x7A', 4 , FRX, P_AE},
  {{'S' , 'E' , ' ' , ' ' , ' '} , '\x7B', 4 , FRX, P_SE}
};
const int NOP = sizeof(T_MOP);

int little_endian()
{
  int n = 1;
  return (*(char *)&n == 1);
}

void change_endian(int * num)
{
  assert(sizeof(int) == 4);
  int b0, b1, b2, b3;
  int val = *num;
  b0 = (val & 0x000000ff) << 24u;
  b1 = (val & 0x0000ff00) << 8u;
  b2 = (val & 0x00ff0000) >> 8u;
  b3 = (val & 0xff000000) >> 24u;
  *num = b0 | b1 | b2 | b3;
}

int lookup_op(unsigned char codop)
{
  int i = 0;
  for(i = 0; i < NOP; i++)
  {
    if(codop == T_MOP[i].CODOP)
    {
      break;
    }
  }
  return i;
}

unsigned long ADDR()
{
  return VR[RR_RX.B] + VR[RR_RX.X] + RR_RX.D;
}

int P_STE()
{
  char bytes[4];
  memcpy(bytes, &FVR[RR_RX.R1].FLOAT_VR.VALUE, 4);
  if(little_endian())
  {
    change_endian((int *)bytes);
  }
  memcpy(OBLZ + ADDR() - POINT.BAS_ADDR, bytes, 4);
  return 0;
}

int P_LE()
{
  char bytes[4];
  memcpy(bytes, OBLZ + ADDR() - POINT.BAS_ADDR, 4);
  if(little_endian())
  {
    change_endian((int *)bytes);
  }
  FVR[RR_RX.R1].FLOAT_VR.VALUE = *(float *)bytes;
  return 0;
}

int P_AE()
{
  char bytes[4];
  memcpy(bytes, OBLZ + ADDR() - POINT.BAS_ADDR, 4);
  if(little_endian())
  {
    change_endian((int *)bytes);
  }
  FVR[RR_RX.R1].FLOAT_VR.VALUE += *(float *)bytes;
  return 0;
}

int P_SE()
{
  char bytes[4];
  memcpy(bytes, OBLZ + ADDR() - POINT.BAS_ADDR, 4);
  if(little_endian())
  {
    change_endian((int *)bytes);
  }
  FVR[RR_RX.R1].FLOAT_VR.VALUE -= *(float *)bytes;
  return 0;
}

int P_BALR(void)
{
  if(RR_RX.R2 != 0)
  {
    CURR_ADDR = VR[RR_RX.R2];
  }
  if(RR_RX.R1 != 0)
  {
    VR[RR_RX.R1] = CURR_ADDR;
  }
  return 0;
}

int P_BCR(void)
{
  int ret = 1;
  if(RR_RX.R1 == 15)
  {
    if((VR[RR_RX.R2] != 0) && (RR_RX.R2 != 0))
    {
      CURR_ADDR = VR[RR_RX.R2];
    }
    else
    {
      if(RR_RX.R2 != 0)
      {
        waddstr(wcyan, "Finish tracking");
        wrefresh(wcyan);
        ret = 1;
      }
    }
  }
  return ret;
}

int P_ST()
{
  char bytes[4];
  memcpy(bytes, (int *)(VR + RR_RX.R1), sizeof(bytes));
  if(little_endian())
  {
    change_endian((int *)bytes);
  }
  memcpy(OBLZ + ADDR() - POINT.BAS_ADDR, bytes, sizeof(bytes));
  return 0;
}

int P_L()
{
  char bytes[4];
  memcpy(bytes, OBLZ + ADDR() - POINT.BAS_ADDR, sizeof(bytes));
  if(little_endian())
  {
    change_endian((int *)bytes);
  }
  memcpy(VR + RR_RX.R1, bytes, sizeof(bytes));
  return 0;
}

int P_A()
{
  char bytes[4];
  memcpy(bytes, OBLZ + ADDR() - POINT.BAS_ADDR, sizeof(bytes));
  if(little_endian())
  {
    change_endian((int *)bytes);
  }
  VR[RR_RX.R1] = VR[RR_RX.R1] + *(int *)(bytes);
  return 0;
}

int P_S()
{
  char bytes[4];
  memcpy(bytes, OBLZ + ADDR() - POINT.BAS_ADDR, sizeof(bytes));
  if(little_endian())
  {
    change_endian((int *)bytes);
  }
  VR[RR_RX.R1] = VR[RR_RX.R1] - *(int *)(bytes);
  return 0;
}

int FRR(void)
{
  int i = lookup_op(INST[0]);
  if(i == NOP)
  {
    return 6;
  }
  waddstr(wgreen, "      ");

  for(int j = 0; j < 5; j++)
  {
    waddch(wgreen, T_MOP[i].MNCOP[j]);
  }
  waddstr(wgreen, " ");
  RR_RX.R1 = INST[1] / 16;
  RR_RX.R2 = INST[1] % 16;
  wprintw(wgreen, "%1d, ", RR_RX.R1);
  wprintw(wgreen, "%1d\n", RR_RX.R2);
  return 0;
}

int FRX(void)
{
  int i = lookup_op(INST[0]);
  if(i == NOP)
  {
    return 6;
  }
  waddstr(wgreen, "  ");
  for(int j = 0; j < 5; j++)
  {
    waddch(wgreen, T_MOP[i].MNCOP[j]);
  }
  waddstr(wgreen, " ");
  RR_RX.R1 = INST[1] >> 4;
  RR_RX.D = (INST[2] % 16 * 256) + INST[3];
  RR_RX.X = INST[1] % 16;
  RR_RX.B = INST[2] >> 4;
  wprintw(wgreen, "%.1d, ", RR_RX.R1);
  wprintw(wgreen, "X'%.3X'(", RR_RX.D);
  wprintw(wgreen, "%1d, ", RR_RX.X);
  wprintw(wgreen, "%1d)", RR_RX.B);
  wprintw(wgreen,"        %.06lX       \n", ADDR());
  if(ADDR() % 4)
  {
    return 7;
  }
  return 0;
}

int wind(void)
{
  for(int i = 0; i < 8; i++)
  {
    wprintw(wred, "%.06lX: ", LIGHT_ADDR + i * 16);
    for (int j = 0; j < 4; j++)
    {
      for (int k = 0; k < 4; k++)
      {
        wprintw(wred, "%.02X", OBLZ[LIGHT_ADDR - POINT.BAS_ADDR + i * 16  + j * 4 + k]);
      }
      waddstr(wred, " ");
    }

    waddstr(wred, "/* ");
    for (int j = 0; j < 16; j++)
    {
      if(isprint(OBLZ[LIGHT_ADDR - POINT.BAS_ADDR + i * 16 + j]))
      {
        waddch(wred, OBLZ[LIGHT_ADDR - POINT.BAS_ADDR + i * 16 + j]);
      }
      else
      {
        waddstr(wred, ".");
      }
    }
    waddstr(wred, " */");
  }
  wrefresh(wred);
  wclear(wred);
  return 0;
}

int sys(void)
{
  int res;
  int gr_pos_y;
  char wstr[80];

//������ ����
  wmargenta = newwin(1, 80, 24, 0);
  wbkgd(wmargenta, COLOR_PAIR(COLOR_MAGENTA));
  waddstr(wmargenta, "\"PgUp\",\"PgDn\",\"Up\",\"Down\"->Dump view; \"Enter\"->Execute next command");
//������ ���������
  wcyan = newwin(1, 80, 23, 0);
  wbkgd(wcyan, COLOR_PAIR(COLOR_CYAN));

//���� ������� ��������
  wred = newwin(8, 68, 15, 0);
  wbkgd(wred, COLOR_PAIR(COLOR_RED));

//���� ���������
  wblue = newwin(16, 12, 0, 68);
  wbkgd(wblue, COLOR_PAIR(COLOR_BLUE));

  yawblue = newwin(4, 19, 0, 0);
  wbkgd(yawblue, COLOR_PAIR(COLOR_BLUE));

//�����
  gr_pos_y = 14;
  wgreen = newwin(11, 68, gr_pos_y, 0);  //�������� ����� ����
  wbkgd(wgreen, COLOR_PAIR(COLOR_GREEN)); //����� �������� ����


  keypad(wmargenta, TRUE);        //��������� �������������� ����� ����������

  int k = 0;
  BEGIN:

  k = lookup_op(OBLZ[CURR_ADDR - POINT.BAS_ADDR]);
  if(k == NOP)
  {
    return 6;
  }
  wprintw(wgreen, "%.06lX: ", CURR_ADDR);
  memset(INST, 0, sizeof(INST));
  memcpy(INST, OBLZ + CURR_ADDR - POINT.BAS_ADDR, T_MOP[k].DLOP);
  for(int j = 0; j < T_MOP[k].DLOP; j++)
  {
    wprintw(wgreen, "%.02X", OBLZ[CURR_ADDR - POINT.BAS_ADDR + j]);
  }
  if((res = T_MOP[k].BXPROG()) != 0)
  {
    return res;
  }

  if(gr_pos_y > 14 - 11 + 1)
  {
    mvwin(wgreen, gr_pos_y--, 0);
  }
  else
  {
    for(int jj = 0; jj < 11 - 1; jj++)
    {
      mvwinnstr(wgreen, jj + 1, 0, wstr, 67);
      mvwaddnstr(wgreen, jj, 0, wstr, 67);
    }
  }
  wrefresh(wgreen);

  CURR_ADDR += T_MOP[k].DLOP;
  LIGHT_ADDR = CURR_ADDR;

  for (int i = 0; i < 16; i++)
  {
    char reg_name [] = "R0 :";
    reg_name[1] = '0' + (i < 10 ? i : i + 7);
    waddstr(wblue, reg_name);
    wprintw(wblue, "%.08lX", VR[i]);
  }
  wrefresh(wblue);      //����� �� �����
  wclear(wblue);      //������� ���� ���������
  for (int i = 0; i < 4; i++)
  {
    waddstr(yawblue, "F");
    wprintw(yawblue, "%d:", i);
    if(little_endian())
    {
      float temp = FVR[i].FLOAT_VR.VALUE;
      FVR[i].FLOAT_VR.VALUE = FVR[i].FLOAT_VR.FILLER;
      FVR[i].FLOAT_VR.FILLER = temp;
      wprintw(yawblue, "%.16lX", FVR[i].DOUBLE_VR.VALUE);
      temp = FVR[i].FLOAT_VR.VALUE;
      FVR[i].FLOAT_VR.VALUE = FVR[i].FLOAT_VR.FILLER;
      FVR[i].FLOAT_VR.FILLER = temp;
    }
  }
  wrefresh(yawblue);
  wclear(yawblue);
  wind();

  waddstr(wcyan, "Executed command at address ");
  wprintw(wcyan, "%.06lX", CURR_ADDR - T_MOP[k].DLOP);
  waddstr(wcyan, "\n");
  wrefresh(wcyan);
  wclear(wcyan);

  WAIT:
  switch(wgetch(wmargenta))
  {
    case 10:
      {
        goto SKIP;
      }

    case  KEY_UP:
      {
        LIGHT_ADDR -= 16;
        wind();
        goto WAIT;
      }

    case  KEY_DOWN:
      {
        LIGHT_ADDR += 16;
        wind();
        goto WAIT;
      }

    case  KEY_PPAGE:
      {
        LIGHT_ADDR -= 128;
        wind();
        goto WAIT;
      }

    case  KEY_NPAGE:
      {
        LIGHT_ADDR += 128;
        wind();
        goto WAIT;
      }
  }
  goto WAIT;

  SKIP:
  res = T_MOP[k].YABXPROG();
  if(T_MOP[k].CODOP == '\x07')
  {
    getch();
    if(res == 1)
    {
      return 8;
    }
  }

  goto BEGIN;

  delwin(wblue);
  delwin(wred);
  delwin(wgreen);
  delwin(wmargenta);
  delwin(yawblue);
  return 0;
}

void InitCurses(void)
{
  initscr();
  curs_set(0);
  noecho();
  cbreak();
  keypad(stdscr, TRUE);
  start_color();
  if(has_colors())
  {
    init_pair(COLOR_BLUE, COLOR_WHITE, COLOR_BLUE);
    init_pair(COLOR_GREEN, COLOR_BLACK, COLOR_GREEN);
    init_pair(COLOR_RED, COLOR_WHITE, COLOR_RED);
    init_pair(COLOR_CYAN, COLOR_BLACK, COLOR_CYAN);
    init_pair(COLOR_MAGENTA, COLOR_WHITE, COLOR_MAGENTA);
  }
}

size_t safe_strcspn(const char * text, const char * sep, const size_t border)
{
  char buff[border];
  memcpy(buff, text, border - 1);
  buff[border - 1] = 0;
  return strcspn(buff, sep) + 1;
}

int main(int argc, char ** argv)
{
  char err_msg[256] = {0};
  const char err_template[] = "[Error:%d] %s\n";
  int err_code = 0;
  if(argc < 2)
  {
    err_code = argc;
    sprintf(err_msg, err_template, err_code, "Command line error");
    printf("%s", err_msg);
    exit(0);
  }

  unsigned int IOBJC = 0;
  char OBJCARD[NOBJ][80];
  {
    unsigned int ISPIS = 0;
    char SPISOK[NSPIS][80];
    for(int i = 1u; (i < argc) && (ISPIS < sizeof(SPISOK)); ++i)
    {
      char NFIL[30] = "\x0";
      strcpy(NFIL, argv[i]);
      if(strcmp(NFIL + (strlen(NFIL) - 3), "tex"))
      {
        err_code = -1;
        sprintf(err_msg, err_template, err_code, "Unexpected file extension");
        printf("%s", err_msg);
        exit(0);
      }
      strncpy(SPISOK[ISPIS++], NFIL, sizeof(NFIL));
    }

    for(unsigned int i = 0u; i < ISPIS; i++)
    {
      FILE * fp = fopen(SPISOK[i], "rb");
      if(fp == NULL)
      {
        err_code = i;
        sprintf(err_msg, err_template, err_code, "Cannot open tex file");
        printf("%s", err_msg);
        return 0;
      }
      while(!feof(fp))
      {
        fread(OBJCARD[IOBJC++], 80, 1, fp);
        if(IOBJC == NOBJ)
        {
          fclose(fp);
          err_code = i;
          sprintf(err_msg, err_template, err_code, "TXT-cards buffer overflow");
          printf("%s", err_msg);
          return 0;
        }
      }
      fclose(fp);
    }
  }
  POINT.P_OBLZ = OBLZ;
  CURR_ADDR = (POINT.BAS_ADDR >> 3) << 3;
  for(unsigned int i = 0; i < IOBJC; i++)
  {
    if(!memcmp(OBJCARD[i] + 1, "TXT", 3))
    {
      memcpy(TXT.BUF_TXT, OBJCARD[i], 80);
      unsigned long ADOP = TXT.STR_TXT.ADOP[0];
      ADOP = (ADOP << 8) + TXT.STR_TXT.ADOP[1];
      ADOP = (ADOP << 8) + TXT.STR_TXT.ADOP[2];
      ADOP += POINT.BAS_ADDR % 8;
      int K = (TXT.STR_TXT.DLNOP[0] << 8) + TXT.STR_TXT.DLNOP[1];
      memcpy(OBLZ + ADOP, TXT.STR_TXT.OPER, K);
    }
  }

  InitCurses();
  err_code = sys();
  endwin();
  switch(err_code)
  {
    case 6:
      {
        sprintf(err_msg, err_template, err_code, "Unexpected command code");
        printf("%s", err_msg);
        break;
      }
    case 7:
      {
        sprintf(err_msg, err_template, err_code, "Interruption. Address error");
        printf("%s", err_msg);
        break;
      }
    case 8:
      {
        printf("[Success:%d] %s\n", err_code, "Complete");
        break;
      }
  }
  return 0;
}
