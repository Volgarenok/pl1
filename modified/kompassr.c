#define DL_ASSTEXT 20
#define DL_OBJTEXT 50                             /*����� ��'����. ������   */
#define NSYM 10                                   /*������ ����.��������    */
#define _XOPEN_SOURCE
#include <unistd.h>
#include <string.h>                               /*���.��������� ��������. */
#include <stdlib.h>                               /*���.��������.������.����*/
#include <stdio.h>                                /*���.�����.�����.��/���  */
#include <ctype.h>                                /*���.�����.�������.����. */
#include <assert.h>
/*
******* � � � �  ��'������� ����������� ������� ����������
*/

unsigned char PRNMET = 'N';                       /*��������� �������.����� */
unsigned int I3;                                           /*������� �����           */

/*
***** � � � �  ��'������� ���������� ��������� � ������������� 1-�� ���������
*/

/*� � � � � � � �  �����.�*/
int FDC();                                        /*�����.���.��.����.DC    */
/*..........................................................................*/
/*� � � � � � � �  �����.�*/
int FDS();                                        /*�����.���.��.����.DS    */
/*..........................................................................*/
/*� � � � � � � �  �����.�*/
int FEND();                                       /*�����.���.��.����.END   */
/*..........................................................................*/
/*� � � � � � � �  �����.�*/
int FEQU();                                       /*�����.���.��.����.EQU   */
/*..........................................................................*/
/*� � � � � � � �  �����.�*/
int FSTART();                                     /*�����.���.��.����.START */
/*..........................................................................*/
/*� � � � � � � �  �����.�*/
int FUSING();                                     /*�����.���.��.����.USING */
/*..........................................................................*/
/*� � � � � � � �  �����.�*/
int FRR();                                        /*�����.���.����.RR-����. */
/*..........................................................................*/
/*� � � � � � � �  �����.�*/
int FRX();                                        /*�����.���.����.RX-����. */
/*..........................................................................*/

/*
***** � � � �  ��'������� ���������� ��������� � ������������� 2-�� ���������
*/

/*� � � � � � � �  �����.�*/
int SDC();                                        /*�����.���.��.����.DC    */
/*..........................................................................*/
/*� � � � � � � �  �����.�*/
int SDS();                                        /*�����.���.��.����.DS    */
/*..........................................................................*/
/*� � � � � � � �  �����.�*/
int SEND();                                       /*�����.���.��.����.END   */
/*..........................................................................*/
/*� � � � � � � �  �����.�*/
int SEQU();                                       /*�����.���.��.����.EQU   */
/*..........................................................................*/
/*� � � � � � � �  �����.�*/
int SSTART();                                     /*�����.���.��.����.START */
/*..........................................................................*/
/*� � � � � � � �  �����.�*/
int SUSING();                                     /*�����.���.��.����.USING */
/*..........................................................................*/
/*� � � � � � � �  �����.�*/
int SRR();                                        /*�����.���.����.RR-����. */
/*..........................................................................*/
/*� � � � � � � �  �����.�*/
int SRX();                                        /*�����.���.����.RX-����. */
/*..........................................................................*/

/*
******* � � � �  ��'������� ������ ���� ������ �����������
*/

/*
******* ��'������� ��������� ������ (�����) ��������� ������
*/

struct ASSKARTA                                  /*������.����� ���������� */
{
  char METKA    [ 8];                  /*���� �����              */
  char PROBEL1  [ 1];                  /*������-�����������      */
  char OPERAC   [ 5];                  /*���� ��������           */
  char PROBEL2  [ 1];                  /*������-�����������      */
  char OPERAND  [12];                  /*���� ��������           */
  char PROBEL3  [ 1];                  /*������ �����������      */
  char COMM     [52];                  /*���� �����������        */
};

/*
******* ��������� ��������� ����� ���. ������ �� ������� �����
*/

union                                            /*���������� ��'��������  */
{
  char BUFCARD [80];                    /*����� �����.���.������  */
  struct ASSKARTA STRUCT_BUFCARD;                /*��������� ����.�� ����� */
} TEK_ISX_KARTA;

/*
***** ������� �������������� ������ (�������� ������������ ���� )
*/

int CHADR;                                       /*�������                 */

/*
***** ������� ��������
*/

int ITSYM = -1;                                  /*���.����.���. ����.����.*/
struct TSYM                                      /*������.������ ����.����.*/
{
  char IMSYM [8];                       /*��� �������             */
  int ZNSYM;                           /*�������� �������        */
  int DLSYM;                           /*����� �������           */
  char PRPER;                           /*������� �����������     */
};
struct TSYM T_SYM[NSYM];                        /*����������� ����.����.  */

/*
***** ������� �������� ��������
*/

struct TMOP                                      /*������.���.����.���.����*/
{
  unsigned char MNCOP[5];                       /*�������� ��������       */
  unsigned char CODOP;                       /*�������� ��� ��������   */
  unsigned char DLOP;                       /*����� �������� � ������ */
  int (*BXPROG1)();                       /*��������� �� �����.�����*/
  int (*BXPROG2)();
} T_MOP []  =                                /*��'������� ����.���.����*/
{
  {{'B','A','L','R',' '} , '\x05' , 2 , FRR, SRR} , /*�������������           */
  {{'B','C','R',' ',' '} , '\x07' , 2 , FRR, SRR} , /*�����                   */
  {{'S','T',' ',' ',' '} , '\x50' , 4 , FRX, SRX} , /*�������                 */
  {{'L',' ',' ',' ',' '} , '\x58' , 4 , FRX, SRX} , /*��������                */
  {{'A',' ',' ',' ',' '} , '\x5A' , 4 , FRX, SRX} , /*��������                */
  {{'S',' ',' ',' ',' '} , '\x5B' , 4 , FRX, SRX} , /*                        */
  {{'S','T','E',' ',' '} , '\x70' , 4 , FRX, SRX} ,
  {{'L','E',' ',' ',' '} , '\x78' , 4 , FRX, SRX} ,
  {{'A','E',' ',' ',' '} , '\x7A' , 4 , FRX, SRX} ,
  {{'S','E',' ',' ',' '} , '\x7B' , 4 , FRX, SRX} ,
};

/*
***** ������� ��������������
*/
struct TPOP                                      /*������.���.����.��.��e�.*/
{
  unsigned char MNCPOP[5];                       /*�������� �������������� */
  int (*BXPROG1)();                       /*��������� �� �����.�����*/
  int (*BXPROG2)();
} T_POP [] =                                /*��'������� ����.��������*/
{
  {{'D','C',' ',' ',' '} , FDC   , SDC   },            /*�������������           */
  {{'D','S',' ',' ',' '} , FDS   , SDS   },            /*�����                   */
  {{'E','N','D',' ',' '} , FEND  , SEND  },            /*�������                 */
  {{'E','Q','U',' ',' '} , FEQU  , SEQU  },            /*��������������          */
  {{'S','T','A','R','T'} , FSTART, SSTART},            /*                        */
  {{'U','S','I','N','G'} , FUSING, SUSING}             /*                        */
};

/*
***** ������� ������� ���������
*/
struct TBASR                                     /*������.���.����.���.���.*/
{
  int SMESH;                                     /*                        */
  char PRDOST;                                   /*                        */
} T_BASR[15] =                                  /*                        */
{
  {0x00,'N'},                                  /*�������������           */
  {0x00,'N'},                                  /*�����                   */
  {0x00,'N'},                                  /*�������                 */
  {0x00,'N'},                                  /*�������                 */
  {0x00,'N'},                                  /*���������               */
  {0x00,'N'},                                  /*                        */
  {0x00,'N'},                                  /*                        */
  {0x00,'N'},                                  /*                        */
  {0x00,'N'},                                  /*                        */
  {0x00,'N'},                                  /*                        */
  {0x00,'N'},                                  /*                        */
  {0x00,'N'},                                  /*                        */
  {0x00,'N'},                                  /*                        */
  {0x00,'N'},                                  /*                        */
  {0x00,'N'}                                   /*                        */
};

/*
***** � � � �   ��'������� ������� � ��'������ �������
*/

unsigned char OBJTEXT [DL_OBJTEXT][80];         /*������ ��'������ ����   */
int ITCARD = 0;                                 /*��������� �����.�����   */

struct OPRR                                     /*������.���.����.����.RR */
{
  unsigned char OP;                             /*��� ��������            */
  unsigned char R1R2;
};

union                                           /*���������� ��'��������  */
{
  unsigned char BUF_OP_RR[2];                  /*��������� �����         */
  struct OPRR OP_RR;                            /*��������������� ���     */
} RR;

struct OPRX                                     /*������.���.����.����.RX */
{
  unsigned char OP;                             /*��� ��������            */
  unsigned char R1X2;                           /*R1 - ������ �������     */
  short B2D2;                                     /*X2 - ������ �������     */
};

union                                           /*���������� ��'��������  */
{
  unsigned char BUF_OP_RX[4];                  /*��������� �����         */
  struct OPRX OP_RX;                            /*��������������� ���     */
} RX;

struct STR_BUF_ESD                              /*������.������ ����� ESD */
{
  unsigned char POLE1      ;                    /*����� ��� ���� 0x02     */
  unsigned char POLE2  [ 3];                    /*���� ���� ��'����.����� */
  unsigned char POLE3  [ 6];                    /*�������                 */
  unsigned char POLE31 [ 2];                    /*����� ������ �� �����   */
  unsigned char POLE32 [ 2];                    /*�������                 */
  unsigned char POLE4  [ 2];                    /*�����.��-� ����� �����. */
  unsigned char IMPR   [ 8];                    /*��� ���������           */
  unsigned char POLE6      ;                    /*��� ���� ESD-�����      */
  unsigned char ADPRG  [ 3];                    /*�������.����� ��������� */
  unsigned char POLE8      ;                    /*�������                 */
  unsigned char DLPRG  [ 3];                    /*����� ���������         */
  unsigned char POLE10 [40];                    /*�������                 */
  unsigned char POLE11 [ 8];                    /*����������������� ����  */
};

struct STR_BUF_TXT                               /*������.������ ����� TXT */
{
  unsigned char POLE1      ;                    /*����� ��� ���� 0x02     */
  unsigned char POLE2  [ 3];                    /*���� ���� ��'����.����� */
  unsigned char POLE3      ;                    /*������                  */
  unsigned char ADOP   [ 3];                    /*�������.����� ��������  */
  unsigned char POLE5  [ 2];                    /*�������                 */
  unsigned char DLNOP  [ 2];                    /*����� ��������          */
  unsigned char POLE7  [ 2];                    /*�������                 */
  unsigned char POLE71 [ 2];                    /*���������� �����.�����. */
  unsigned char OPER   [56];                    /*���� ��������           */
  unsigned char POLE9  [ 8];                    /*����������������� ����  */
};

struct STR_BUF_END                                /*������.������ ����� END */
{
  unsigned char POLE1      ;                    /*����� ��� ���� 0x02     */
  unsigned char POLE2  [ 3];                    /*���� ���� ��'����.����� */
  unsigned char POLE3  [68];                    /*�������                 */
  unsigned char POLE9  [ 8];                    /*����������������� ����  */
};

union                                           /*���������� ��'��������  */
{
  struct STR_BUF_ESD STR_ESD;                   /*��������� ������        */
  unsigned char BUF_ESD [80];                   /*����� ����� ESD         */
} ESD;


union                                           /*���������� ��'��������  */
{
  struct STR_BUF_TXT STR_TXT;                   /*��������� ������        */
  unsigned char BUF_TXT [80];                   /*����� ����� TXT         */
} TXT;

union                                           /*���������� ��'��������  */
{
  struct STR_BUF_END STR_END;                   /*��������� ������        */
  unsigned char BUF_END [80];                   /*����� ����� ESD         */
} END;

const char * parse(const char * text, char * data, const char * sep)
{
  size_t pos = 0;
  pos = strcspn(text, sep);
  data[pos] = '\0';
  strncpy(data, text, pos++);
  text += pos;
  return text;
}

size_t safe_strcspn(const char * text, const char * sep, const size_t border)
{
  char buff[border];
  memcpy(buff, text, border - 1);
  buff[border - 1] = 0;
  return strcspn(buff, sep) + 1;
}

const char * label_parse(const char * text, char label[9])
{
  return parse(text, label, ", ");
}

const char * value_parse(const char * text, char value[12])
{
  return parse(text, value, "' ");
}

int id_starts(const char text)
{
  return isalpha(text) || (text == '_');
}

int lookup_sym(const char label[9])
{
  int j = 0;
  for(j = 0; j <= ITSYM; j++)
  {
    if(!strncmp(T_SYM[j].IMSYM, label, strlen(label)))
    {
      break;
    }
  }
  return j;
}

int little_endian()
{
  int n = 1;
  return (*(char *)&n == 1);
}

void change_endian(int * num)
{
  assert(sizeof(int) == 4);
  int b0, b1, b2, b3;
  int val = *num;
  b0 = (val & 0x000000ff) << 24u;
  b1 = (val & 0x0000ff00) << 8u;
  b2 = (val & 0x00ff0000) >> 8u;
  b3 = (val & 0xff000000) >> 24u;
  *num = b0 | b1 | b2 | b3;
}

/*
******* � � � �  ��'������� �����������, ������������ ��� 1-�� ���������
*/

void addr_round(const int K)
{
  assert(K > 0);
  CHADR = (CHADR / K + ((CHADR % K) ? 1 : 0)) * K;
}

int FDC()                                         /*�����.���.��.����.DC    */
{
  if(PRNMET == 'Y') //����������?
  {
    if(TEK_ISX_KARTA.STRUCT_BUFCARD.OPERAND[0] == 'F')
    {
      addr_round(4);
      T_SYM[ITSYM].DLSYM = 4; //����� �������
      T_SYM[ITSYM].PRPER = 'R'; //������� �����������
      T_SYM[ITSYM].ZNSYM = CHADR;
      PRNMET = 'N'; //������ �������
      CHADR += 4;
    }
    else if(TEK_ISX_KARTA.STRUCT_BUFCARD.OPERAND[0] == 'E')
    {
      addr_round(8);
      T_SYM[ITSYM].DLSYM = 8;
      T_SYM[ITSYM].PRPER = 'R';
      T_SYM[ITSYM].ZNSYM = CHADR;
      PRNMET = 'N';
      CHADR += 8;
    }
    else
    {
      return 1;
    }
  }
  return 0;
}
/*..........................................................................*/
int FDS()                                         /*�����.���.��.����.DS    */
{
  FDC();
  return 0;                                     /*������� ��������� �����.*/
}

/*..........................................................................*/
int FEND()                                        /*�����.���.��.����.END   */
{
  return 100; //����� ������� ���������
}
/*..........................................................................*/
int FEQU()                                        /*�����.���.��.����.EQU   */
{
  if(TEK_ISX_KARTA.STRUCT_BUFCARD.OPERAND[0] == '*')
  {
    T_SYM[ITSYM].ZNSYM = CHADR;
    T_SYM[ITSYM].DLSYM = 1;
    T_SYM[ITSYM].PRPER = 'R';//'R' � ����� PRPER
  }
  else
  {
    T_SYM[ITSYM].ZNSYM = atoi((char*)TEK_ISX_KARTA.STRUCT_BUFCARD.OPERAND);
    T_SYM[ITSYM].DLSYM = 1;
    T_SYM[ITSYM].PRPER = 'A'; //'A' � ���� PRPER
  }
  PRNMET = 'N';
  return 0;
}
/*..........................................................................*/
int FSTART()                                      /*�����.���.��.����.START */
{                                                /*CHADR ���������� ������ */
  CHADR = atoi((char*)TEK_ISX_KARTA.STRUCT_BUFCARD.OPERAND);
  addr_round(8);
  T_SYM[ITSYM].ZNSYM = CHADR;
  T_SYM[ITSYM].DLSYM = 1;
  T_SYM[ITSYM].PRPER = 'R';
  PRNMET = 'N';
  return 0;
}
/*..........................................................................*/
int FUSING()                                      /*�����.���.��.����.USING */
{
  return 0;                                     /*�������� ������.��������*/
}
/*..........................................................................*/
int FRR()                                         /*�����.���.����.RR-����. */
{
  CHADR += 2;                              /*��������� ��.���. �� 2  */
  if(PRNMET == 'Y')                            /*���� ����� �����.�����, */
  {                                              /*�� � ����. ��������:    */
    T_SYM[ITSYM].DLSYM = 2;                       /*��������� ����� ���.����*/
    T_SYM[ITSYM].PRPER = 'R';                     /*� ���������� �����.�����*/
  }
  return 0;                                      /*����� �� ������������   */
}
/*..........................................................................*/
int FRX()                                         /*�����.���.����.RX-����. */
{
  CHADR += 4;                              /*��������� ��.���. �� 4  */
  if(PRNMET == 'Y')                            /*���� ����� �����.�����, */
  {                                              /*�� � ����. ��������:    */
    T_SYM[ITSYM].DLSYM = 4;                       /*��������� ����� ���.����*/
    T_SYM[ITSYM].PRPER = 'R';                     /*� ���������� �����.�����*/
  }
  return 0;                                      /*����� �� ������������   */
}
/*..........................................................................*/

/*
******* � � � �  ��'������� �����������, ������������ ��� 2-�� ���������
*/

void STXT( int ARG )                              /*�����.������.TXT-�����  */
{
  int RAB = CHADR;
  if(little_endian())
  {
    change_endian(&RAB);
  }
  assert(sizeof(int) == 4);
  memcpy(TXT.STR_TXT.ADOP, (char *)&RAB + 1, 3);
  if(ARG == 2)
  {
    memset(TXT.STR_TXT.OPER, 64, 4);
    memcpy(TXT.STR_TXT.OPER, RR.BUF_OP_RR, 2);
    TXT.STR_TXT.DLNOP[1] = 2;
  }
  else if(ARG == 4)
  {
    memcpy(TXT.STR_TXT.OPER, RX.BUF_OP_RX, 4);
    TXT.STR_TXT.DLNOP[1] = 4;
  }
  else if(ARG == 8)
  {
    memcpy(TXT.STR_TXT.OPER, RX.BUF_OP_RX, 4);
    memset(TXT.STR_TXT.OPER + 4, 0, 4);
    TXT.STR_TXT.DLNOP[1] = 8;
  }
  memcpy(TXT.STR_TXT.POLE9, ESD.STR_ESD.POLE11, 8);
  memcpy(OBJTEXT[ITCARD++], TXT.BUF_TXT, 80);
  CHADR += ARG;
  return;
}

int SDC()                                         /*�����.���.��.����.DC    */
{
  char *RAB;
  char TYPE[12];
  char OPERAND[12];
  RX.OP_RX.OP   = 0;
  RX.OP_RX.R1X2 = 0;
  const char * text = TEK_ISX_KARTA.STRUCT_BUFCARD.OPERAND;
  text = value_parse(text, TYPE);
  value_parse(text, OPERAND);
  if(!strncmp(TYPE, "F", sizeof(TYPE)))
  {
    RX.OP_RX.B2D2 = atoi(OPERAND);
    if(little_endian())
    {
      RAB = (char *) &RX.OP_RX.B2D2;
      char buff = *RAB;
      *RAB = *(RAB + 1);
      *(RAB + 1) = buff;
    }
    addr_round(4);
    STXT(4);
  }
  else if(!strncmp(TYPE, "E", sizeof(TYPE)))
  {
    float value = (float)atof(OPERAND);
    assert(sizeof(float) == sizeof(int));
    if(little_endian())
    {
      change_endian((int *)&value);
    }
    memcpy(&RX.BUF_OP_RX, (char *)&value, 4);
    addr_round(8);
    STXT(8);
  }
  else
  {
    return 1;
  }
  return 0;
}
/*..........................................................................*/
int SDS()                                         /*�����.���.��.����.DS    */
{
  RX.OP_RX.OP = 0;
  RX.OP_RX.R1X2 = 0;
  if(TEK_ISX_KARTA.STRUCT_BUFCARD.OPERAND[0]=='F')
  {
    RX.OP_RX.B2D2 = 0;
  }
  else
  {
    return 1;
  }
  STXT(4);
  return 0;
}
/*..........................................................................*/
int SEND()                                        /*�����.���.��.����.END   */
{
  memcpy(END.STR_END.POLE9, ESD.STR_ESD.POLE11, 8);
  memcpy(OBJTEXT[ITCARD++], END.BUF_END, 80);
  return 100;
}
/*..........................................................................*/
int SEQU()                                        /*�����.���.��.����.EQU   */
{
  return 0;
}
/*..........................................................................*/
int SSTART()                                      /*�����.���.��.����.START */
{
  char METKA[9];
  int RAB;
  label_parse(TEK_ISX_KARTA.STRUCT_BUFCARD.METKA, METKA);
  int j = lookup_sym(METKA);
  if(j > ITSYM)
  {
    return 2;
  }
  RAB = CHADR - T_SYM[j].ZNSYM; //����� ���������
  if(little_endian())
  {
    change_endian(&RAB);
  }
  assert(sizeof(int) == 4);
  memcpy(ESD.STR_ESD.DLPRG, (char *)&RAB + 1, 3);
  CHADR = T_SYM[j].ZNSYM; //������������� ������ (� ������������ �� START)
  {
    int addr = CHADR;
    if(little_endian())
    {
      change_endian(&addr);
    }
    memcpy(ESD.STR_ESD.ADPRG, (char *)&addr + 1, 3);
    memcpy(ESD.STR_ESD.IMPR, METKA, strlen(METKA));
    memcpy(ESD.STR_ESD.POLE11, METKA, strlen(METKA));
    memcpy(OBJTEXT[ITCARD++], ESD.BUF_ESD, 80);
  }
  return 0;
}
/*..........................................................................*/
int SUSING()                                      /*�����.���.��.����.USING */
{
  char METKAS[2][9];
  int NBASRG;
  const char * text = TEK_ISX_KARTA.STRUCT_BUFCARD.OPERAND;
  text = label_parse(text, METKAS[0]);
  label_parse(text, METKAS[1]);

  if(id_starts(*METKAS[1]))
  {
    int j = lookup_sym(METKAS[1]);
    if(j > ITSYM)
    {
      return 2; //������������� �������������
    }
    NBASRG = T_SYM[j].ZNSYM;
  }
  else
  {
    NBASRG = atoi(METKAS[0]);
  }
  if(NBASRG > 0x0f)
  {
    return 6; //������ ��������� ����
  }

  T_BASR [NBASRG - 1].PRDOST = 'Y'; //������ ������� ����������
  if(*METKAS[0] == '*')
  {
    T_BASR[NBASRG - 1].SMESH = CHADR;//������� �������� ������ CHADR
  }
  else
  {
    int j = lookup_sym(METKAS[0]);
    if(j > ITSYM)
    {
      return 2; //������������� �������������
    }
    T_BASR[NBASRG - 1].SMESH = T_SYM[j].ZNSYM;
  }
  return 0;
}

/*..........................................................................*/
int SRR()                                         /*�����.���.����.RR-����. */
{
  const int amount = 2;
  char METKAS[amount][9];
  unsigned char R1R2 = 0;
  RR.OP_RR.OP = T_MOP[I3].CODOP; //��� ��������
  const char * text = TEK_ISX_KARTA.STRUCT_BUFCARD.OPERAND;
  text = label_parse(text, METKAS[0]);
  label_parse(text, METKAS[1]);
  for(int i = 0; i < amount; ++i)
  {
    if(id_starts(*METKAS[i]))
    {
      int j = lookup_sym(METKAS[i]);
      if(j > ITSYM)
      {
        return 2; //������������� �������������
      }
      R1R2 += T_SYM[j].ZNSYM << 4 * ((amount - 1) - i);
    }
    else
    {
      R1R2 += atoi(METKAS[i]) << 4 * ((amount - 1) - i);
    }
  }
  RR.OP_RR.R1R2 = R1R2; //�������� �������� �������
  STXT(2);
  return 0;
}

/*..........................................................................*/
int SRX()                                         /*�����.���.����.RX-����. */
{
  char METKAS[2][9];
  int  DELTA;
  int  ZNSYM;
  int  NBASRG;
  unsigned char R1X2;
  int B2D2;
  RX.OP_RX.OP = T_MOP[I3].CODOP;
  const char * text = TEK_ISX_KARTA.STRUCT_BUFCARD.OPERAND;
  text = label_parse(text, METKAS[0]);
  label_parse(text, METKAS[1]);

  if(id_starts(*METKAS[0]))
  {
    int j = lookup_sym(METKAS[0]);
    if(j > ITSYM)
    {
      return 2; //������������� �������������
    }
    R1X2 = T_SYM[j].ZNSYM << 4;
  }
  else
  {
    R1X2 = atoi(METKAS[0]) << 4;
  }

  if(id_starts(*METKAS[1]))
  {
    int j = lookup_sym(METKAS[1]);
    if(j > ITSYM)
    {
      return 2;
    }
    NBASRG = 0; //����� �������� ��������
    DELTA = 0xfff - 1; //��������
    ZNSYM = T_SYM[j].ZNSYM; //�������� ������� ��������

    int i = 0;
    for (i = 0; i < 15; i++) //���� ������� �������
    {
      if(T_BASR[i].PRDOST == 'Y' && //��������
         ZNSYM - T_BASR[i].SMESH >= 0 && //������� �� �������� ��������
         ZNSYM - T_BASR[i].SMESH < DELTA) //�������� ������� �
                                          //�������� ������� ��������
      {
        NBASRG = i + 1;
        DELTA  = ZNSYM - T_BASR[i].SMESH;
        //DELTA  = ZNSYM;
      }
    }
    if(NBASRG == 0 || DELTA > 0xfff)
    {
      return 5; //�� ������� ������� ������� �������
    }
    else
    {
      B2D2 = NBASRG << 12; //������ ������� � ���� B2D2
      B2D2 = B2D2 + DELTA;
      if(little_endian())
      {
        change_endian(&B2D2);
      }
      memcpy(&RX.OP_RX.B2D2, (char *)&B2D2 + 2, 2);
    }
  }
  else
  {
    return 4; //��ң� �������� ��������� �������
  }

  RX.OP_RX.R1X2 = R1X2;
  STXT(4); //������������ TXT-�����
  return 0;
}
/*..........................................................................*/
int SOBJFILE(char * fname)                                    /*��������.������.��'���. */
{                                                /*�����                   */
  fname[strlen(fname) - 3] = '\x0';
  strcat(fname, "tex");
  FILE * fp = fopen(fname, "wb");
  if(!fp)
  {
    return -7;
  }
  int RAB2 = fwrite (OBJTEXT, 80, ITCARD, fp);
  fclose(fp);
  return RAB2;
}

/*..........................................................................*/
void INITUNION ()
{
  ESD.STR_ESD.POLE1 =  0x02;
  memcpy(ESD.STR_ESD.POLE2, "ESD", 3);
  memset(ESD.STR_ESD.POLE3, 0x40, 6);
  ESD.STR_ESD.POLE31[0] = 0x00;
  ESD.STR_ESD.POLE31[1] = 0x10;
  memset(ESD.STR_ESD.POLE32, 0x40, 2);
  ESD.STR_ESD.POLE4[0] = 0x00;
  ESD.STR_ESD.POLE4[1] = 0x01;
  memset(ESD.STR_ESD.IMPR, 0x40, 8);
  ESD.STR_ESD.POLE6 = 0x00;
  memset(ESD.STR_ESD.ADPRG, 0x00, 3);
  ESD.STR_ESD.POLE8 = 0x40;
  memset(ESD.STR_ESD.DLPRG, 0x00, 3);
  memset(ESD.STR_ESD.POLE10, 0x40, 40);
  memset(ESD.STR_ESD.POLE11, 0x40, 8);

  TXT.STR_TXT.POLE1 = 0x02;
  memcpy(TXT.STR_TXT.POLE2, "TXT", 3);
  TXT.STR_TXT.POLE3 =  0x40;
  memset(TXT.STR_TXT.ADOP, 0x00, 3);
  memset(TXT.STR_TXT.POLE5, 0x40, 2);
  memset(TXT.STR_TXT.DLNOP, 0X00, 2);
  memset(TXT.STR_TXT.POLE7, 0x40, 2);
  TXT.STR_TXT.POLE71[0] = 0x00;
  TXT.STR_TXT.POLE71[1] = 0x01;
  memset(TXT.STR_TXT.OPER, 0x40, 56);
  memset(TXT.STR_TXT.POLE9, 0x40, 8);

  END.STR_END.POLE1 = 0x02;
  memcpy(END.STR_END.POLE2, "END", 3);
  memset(END.STR_END.POLE3, 0x40, 68);
  memset(END.STR_END.POLE9, 0x40, 8);

}

int main( int argc, char **argv )
{
  char YA_ASSTEXT[DL_ASSTEXT * 80];
  char err_msg[256] = {0};
  const char err_template[] = "[Error:%d] %s\n";
  int err_code = 0;
  INITUNION(); //��������� ���������� ����

  if(argc != 2)
  {
    printf("[User error] Expected 2 command line arguments\n");
    return 0;
  }

  if(strcmp(argv[1] + strlen(argv[1]) - 3, "ass"))
  {
    printf("[User error] Wrong file extension\n");
    return 0;
  }

  FILE *fp = fopen(argv[1], "rb");
  if(!fp)
  {
    printf ("[User error] Cannot find file: %s\n", argv[1]);
    return 0;
  }
  const int count = fread(YA_ASSTEXT, 1, sizeof(YA_ASSTEXT), fp);
  if(!feof(fp) && count == sizeof(YA_ASSTEXT))
  {
    printf("[User error] Buffer overflow\n");
    fclose(fp);
    return 0;
  }
  YA_ASSTEXT[count] = 0;
  fclose(fp);

  //������ ��������
  const char * token = YA_ASSTEXT;
  int I1 = 0;
  for(I1 = 0; I1 < DL_ASSTEXT && (err_code == 0); I1++)
  {
    int pos = safe_strcspn(token, "\n\0", 80);
    memset(TEK_ISX_KARTA.BUFCARD, 0, 80);
    memcpy(TEK_ISX_KARTA.BUFCARD, token, pos); //������ � �����
    token += pos;
    if(TEK_ISX_KARTA.STRUCT_BUFCARD.METKA[0] != ' ') //��� �����
    {
      ++ITSYM;
      PRNMET = 'Y'; //���� �����
      memcpy(T_SYM[ITSYM].IMSYM, TEK_ISX_KARTA.STRUCT_BUFCARD.METKA , 8); //�����
      T_SYM[ITSYM].ZNSYM = CHADR; //�����
    }

    unsigned int i = 0;
    for(i = 0; i < sizeof(T_POP); i++) //��������������
    {
      if(!memcmp(TEK_ISX_KARTA.STRUCT_BUFCARD.OPERAC, T_POP[i].MNCPOP, 5))
      {
        break;
      }
    }
    if(i < sizeof(T_POP))
    {
      err_code = T_POP[i].BXPROG1();
      switch(err_code)
      {
        case 1:
          sprintf(err_msg, err_template, err_code, "Data format error");
      }
      continue;
    }

    for(I3 = 0; I3 < sizeof(T_MOP); I3++) //��������
    {
      if(!memcmp(TEK_ISX_KARTA.STRUCT_BUFCARD.OPERAC, T_MOP[I3].MNCOP, 5))
      {
        break;
      }
    }
    if(I3 < sizeof(T_MOP))
    {
        T_MOP[I3].BXPROG1();
        PRNMET = 'N'; //������ ������� �����
        continue;
    }
    err_code = 3;
    sprintf(err_msg, err_template, 3, "Operation code error");
  }
  if(err_code != 100)
  {
    sprintf(err_msg, err_template, err_code, "Unregistered");
    goto CONT6;
  }
  err_code = 0;

  //������ ��������
  token = YA_ASSTEXT;
  for(I1 = 0; I1 < DL_ASSTEXT && (err_code == 0); I1++)
  {
    int pos = safe_strcspn(token, "\n\0", 80);
    memset(TEK_ISX_KARTA.BUFCARD, 0, 80);
    memcpy(TEK_ISX_KARTA.BUFCARD, token, pos); //������ � �����
    token += pos;
    //��������������
    unsigned int i = 0;
    for(i = 0; i < sizeof(T_POP); i++)
    {
      if(!memcmp(TEK_ISX_KARTA.STRUCT_BUFCARD.OPERAC, T_POP[i].MNCPOP, 5))
      {
        break;
      }
    }
    if(i < sizeof(T_POP))
    {
      err_code = T_POP[i].BXPROG2();
      continue;
    }
    //��������
    for(I3 = 0; I3 < sizeof(T_MOP); I3++)
    {
      if(!memcmp(TEK_ISX_KARTA.STRUCT_BUFCARD.OPERAC, T_MOP[I3].MNCOP, 5))
      {
        break;
      }
    }
    if(I3 < sizeof(T_MOP))
    {
      err_code = T_MOP[I3].BXPROG2();
      switch(err_code)
      {
        case 2:
          sprintf(err_msg, err_template, err_code, "Undeclared id");
          break;
        case 4:
          sprintf(err_msg, err_template, err_code, "Second operand error");
          break;
        case 5:
          sprintf(err_msg, err_template, err_code, "Basing error");
          break;
        case 6:
          sprintf(err_msg, err_template, err_code, "Forbidden regiseter");
          break;
        case 7:
          sprintf(err_msg, err_template, err_code, "Object file opening failed");
          break;
      }
    }
  }

  CONT6:
  if(err_code != 100)
  {
    printf("%s", err_msg);
    printf("[Error:%d] Happend in card %d\n", err_code, I1);
  }
  else
  {
    printf("[Success:%d] Translation complete\n", err_code);
    err_code = SOBJFILE(argv[1]) != ITCARD;
    if(err_code)
    {
      printf("[Error] Building TEX file (object) failed\n");
    }
    else
    {
      printf("[Success:%d] Object file ready\n", err_code);
    }
  }
  return 0;
}
