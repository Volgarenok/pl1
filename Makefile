all: komppl_0 kompassr_0 absloadm_0 komppl_1 kompassr_1 absloadm_1

komppl_0: legacy/komppl.c
	gcc -g -m32 -o $@ $^

kompassr_0: legacy/kompassr.c
	gcc -g -m32 -o $@ $^

absloadm_0: legacy/absloadm.c
	gcc -g -o $@ $^ -lncurses

komppl_1: modified/komppl.c
	gcc -g -m32 -o $@ $^ -Wall -Wextra -std=c11

kompassr_1: modified/kompassr.c
	gcc -g -m32 -o $@ $^ -Wall -Wextra -std=c11

absloadm_1: modified/absloadm.c
	gcc -g -Wall -Wextra -std=c11 -o $@ $^ -lncurses

clean:
	$(RM) -rf komppl_0 kompassr_0 absloadm_0 komppl_1 kompassr_1 absloadm_1
